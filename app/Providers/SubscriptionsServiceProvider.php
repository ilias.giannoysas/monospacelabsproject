<?php

namespace App\Providers;

use App\Repositories\SubscriptionsRepository;
use App\Repositories\Interfaces\SubscriptionsRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class SubscriptionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SubscriptionsRepository::class, 
            SubscriptionsRepositoryInterface::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
