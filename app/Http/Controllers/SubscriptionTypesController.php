<?php

namespace App\Http\Controllers;

use App\SubscriptionTypes;
use Illuminate\Http\Request;

class SubscriptionTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubscriptionTypes  $subscriptionTypes
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionTypes $subscriptionTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubscriptionTypes  $subscriptionTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(SubscriptionTypes $subscriptionTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubscriptionTypes  $subscriptionTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubscriptionTypes $subscriptionTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubscriptionTypes  $subscriptionTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriptionTypes $subscriptionTypes)
    {
        //
    }
}
