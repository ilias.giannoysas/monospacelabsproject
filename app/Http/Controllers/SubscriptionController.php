<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Repositories\Interfaces\SubscriptionsRepositoryInterface;

class SubscriptionController extends Controller
{
    private $subscriptionsRepository;

    public function __construct(SubscriptionsRepositoryInterface $subscriptionsRepository)
    {
        $this->subscriptionsRepository = $subscriptionsRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getDataFromApi()
    {
        $client = new Client();
        
        $response = $client->request('GET', 'http://giannoysas.front.challenge.dev.monospacelabs.com/users');
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        
        return $body;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $email = request('email');
        $price = request('price');
        $from = request('from');
        $to = request('to');

        $subscriptions = json_decode($this->getDataFromApi());

        if($email !== null && $price > 0 && $from !== null && $to !== null) 
        {
            $newSubscription = findByEmail($email,$subscriptions);
            if($newSubscription !== null)  
            {
                $this->subscriptionsRepository->create([
                    'type'=>$newSubscription->type,
                    'user_id'=>$newSubscription->id,
                    'price'=> 0.3 * $price ,
                    'from'=>$from,
                    'to'=>$to
                ]);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }
}
