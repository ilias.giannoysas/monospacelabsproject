<?php

namespace App\Repositories\Interfaces;

use App\User;

interface SubscriptionsRepositoryInterface
{
    public function all();
    public function create(array $attributes);
    public function findByEmail(string $email);
    public function filterBydate(datetime $from,datetime $to);
}

