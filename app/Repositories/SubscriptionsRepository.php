<?php

namespace App\Repositories;

use App\Models\Subscription;
use App\Models\SubscriptionTypes;
use App\User;
use App\Repositories\Interfaces\SubscriptionsRepositoryInterface;

class SubscriptionsRepository implements SubscriptionsRepositoryInterface
{
    public function all()
    {
        return Subscription::all();
    }

    public function create($attributes)
    {
        return Subscription::create($attributes);
    }

    public function findByEmail(string $email,$apiSubscriptions)
    {
        $subscriptions = Subscription::where('subscription.email',$email)
        ->join('subscription_types', 'subscription_types.id', '=', 'subscription.subscription_type_id ')
        ->where('subscription.active',1)->pluck('subscription_types.name');

        foreach($apiSubscriptions as $apiSubscription) 
        {
            if($email == $apiSubscription->email && $apiSubscription->active == 1) 
            {
                foreach($subscriptions as $subscription)
                {
                    if($subscription != $apiSubscription->Type) return $apiSubscription;
                    else return false;
                }
            }
        }
    }
}