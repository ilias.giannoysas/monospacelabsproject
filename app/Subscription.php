<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

    protected $fillable = [
        'title', 'content', 'user_id','subscription_type_id','id','from','to'
    ];

    public function subscriptionTypes()
    {
        return $this->hasOne(SybscriptionTypes::class);
    }
}
